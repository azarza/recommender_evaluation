# Recommender Evaluation and Methodology of Testing

Here you can find a brief explanation on the test that were carried out to attain the data and the way one can replicate them.

## Test 1: Information for a single user

For the first test and collection of data I ran the script on a single randomized user and collected data from all aprt of the code. The data collected in was :

1. Time in seconds to load the data using a pandas dataframe.
2. Time in seconds to fit the KNN model on the given data.
3. Time in seconds to do the predictions and recommendation for the given user once the data had been loaded and the KNN model was fitted to the data. 
4. Memory spaced used in gigabytes by the process of the python script.
5. Time taken to execute the whole program

To attain this data one can indenpendetly use command line arguments. The exact command used when running this test is:

```
python3 ratings.py --test1 -s
```

This command line arguments are can be found by running `python3 ratings_1M.py -h` or `python3 ratings_1M.py --help`. 


The arguments ran here to attain the data are :

- `--test1`: This command will print the information for test 1. 

- `-s` or `--seed`: Input a seed for testing (This can be ommited when running test 1 to manually select user). 


## Test 2: Throughput for 100 users

For the second test I ran, I randomized the index of 100 users with a seed increasing from 0 - number of users and made recommendations for all of them. Once the recommendation was done I recorded the time it took to do the whole recommendation process. The command run for this test is:

```
python3 ratings.py --test2 -s
```

Here the command line argument `--test2`runs the recommendation process for 100 randomized users set by the seed of each with the command `-s`. This does not take into account the loading of the data or the fitting of the KNN as in this case they are both identical for all the users. 

The argument ran here to attain the data is:

- `--test2`: Runs and prints the time taken for the prediction process of 100 users as well as the average per user.

The command line arguments can be found by running `python3 ratings_1M.py -h` or `python3 ratings_1M.py --help`. 

*NOTE :* for both of this tests the command - `-d` or - `--debug` is available to pring out the actual ratings and the top k nearest neigbours with their distance from the user.

# Scaling Testing

For the scaling section of the report tests were ran on the script *ratings_blocking.py* with the various datasets that can be found in the __datasets__ folder.

For the tests the command line areguments are :


- `--dataset`: Which determines the file path required for the ratings dataset. 

- `--k`: Determines the k value in the kNN algorithm.

- `--bsz`: Determines the size of the block used for the block calculations.
  
- `--test1`: This command will print the information for test 1. 

- `--test2`: Runs and prints the time taken for the prediction process of 100 users as well as the average per user.

- `-s` or `--seed`: Input a seed for testing.


## Test 1: Information for a single user
The tests ran to evaluate the time spent on various parts of the script were: 
```
python3 ratings_blocking.py --dataset datasets/ml-1m/ratings.dat --k 21 --bsz 200 -s --test1
```
```
python3 ratings_blocking.py --dataset datasets/ml-10m/ratings.dat --k 21 --bsz 200 -s --test1
```
```
python3 ratings_blocking.py --dataset datasets/ml-20m/ratings.csv --k 21 --bsz 200 -s --test1
```

## Test 2: Throughput for 100 users

The tests ran to evaluate the throughput and time taken to make recommendations for 100 user as well as the average time taken were:

```
python3 ratings_blocking.py --dataset datasets/ml-1m/ratings.dat --k 21 --bsz 200 -s --test2
```
```
python3 ratings_blocking.py --dataset datasets/ml-10m/ratings.dat --k 21 --bsz 200 -s --test2
```
```
python3 ratings_blocking.py --dataset datasets/ml-20m/ratings.csv --k 21 --bsz 200 -s --test2
```
*NOTE :* for both of this tests the command - `-d` or - `--debug` is available to pring out the actual ratings and the top k nearest neigbours with their distance from the user.