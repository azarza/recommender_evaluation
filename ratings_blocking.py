import numpy as np
import pandas as pd
import scipy.sparse as sp
import argparse
import collections
import time
import gc
import random
import os
import psutil
import sys

# Get current process ID.
pid = os.getpid() 
# For this process get psutil methods.
python_process = psutil.Process(pid)

def cosine_sim(R):
    R_sqrt = np.sqrt(np.asarray(R.power(2).sum(1)).squeeze())
    R_sqrt[R_sqrt == 0] = 1.
    ratings = R.transpose().multiply(np.power(R_sqrt, -1)).transpose().tocsr()
    return ratings
    
def topk(S, k):
    topk = sp.lil_matrix(S.shape)
    for i in range(0,S.shape[0]):
        row = S[i,:]
        if len(row.data) > k:
            ind = row.data.argpartition(len(row.data)-k) 
            row.data[ind[:-k]] = 0.
            row.eliminate_zeros()
        topk[i,:] = row
    return topk

parser = argparse.ArgumentParser(description="Neighbourhood-based Recommendation with k-NN for evaluating raspberry pi's perfomance.")
parser.add_argument('--dataset', type=str, default=None, help="Dataset used for recommender system")
parser.add_argument('--k', type=int, default=300,help='Neighbourhood size (default: 300)')
parser.add_argument('--bsz', type=int, default=1000, help='Batch size for similarity computations (default: 1000)')
parser.add_argument("-d", "--debug", default=False, action= "store_true", help="Print recommendations and distances of neighbours.")
parser.add_argument("-s", "--seed", default=False, action="store_true", help="Input a seed for testing.")
parser.add_argument("--test1", default = False,action = "store_true",help="Runs the first test printing time taken for specific parts of the code.")
parser.add_argument("--test2", default = False,action = "store_true",help="Runs the second test printing time taken to make predictions for 100 users.")
args = parser.parse_args()


if args.dataset == 'datasets/ml-1m/ratings.dat' :
    sep = '::' 
    sepF = '::' 
    header_r = None
    header_F = None
    filmset = 'datasets/ml-1m/movies.dat'
elif args.dataset == 'datasets/ml-10m/ratings.dat':
    sep = '::' 
    sepF = '::' 
    header_r = None
    header_F = None
    filmset = 'datasets/ml-10m/movies.dat'
elif args.dataset == 'datasets/ml-20m/ratings.csv':
    sep = ','
    sepF = ','
    header_r = 0
    header_F = 0
    filmset = 'datasets/ml-20m/movies.csv'


start =  time.perf_counter()
film = pd.read_csv(filmset, sep=sepF, header=header_F,
            names=['filmId', 'title'],
            usecols=range(2), encoding='ISO-8859-1', engine='python')
if args.test1 :
    print("Read films data in {0:.2f}s".format(time.perf_counter() - start))

start =  time.perf_counter()
chunksize = 100000
tfr = pd.read_csv(args.dataset, header = header_r, names=['userId', 'filmId', 'rating'],
            usecols=range(3), sep = sep, dtype = {"userId" : np.int32, "filmId": np.int32, "rating": np.int32},
            chunksize=chunksize, iterator=True, engine = 'python')
ratings = pd.concat(tfr, ignore_index=True)


u = np.array(ratings['userId']) - 1
nb_users = len(np.unique(u))
i = np.array(ratings['filmId']) - 1
nb_items = len(np.unique(i))
r = np.array(ratings['rating'])
if args.test1:
    print("Read ratings data in {0:.2f}s".format(time.perf_counter() - start))
#print("Number of users: {}".format(nb_users))
#print("Number of items: {}".format(nb_items))
#print("Number of ratings: {}".format(len(r)))

start = time.perf_counter()
R = sp.csr_matrix((r, (u,i)))
if args.test1:
    print("Created sparse matrix R in {0:.2f}s,".format(time.perf_counter() - start) + "nnz = {}".format(R.nnz))

u_cosine_similarity = cosine_sim(R)
u_cos_sim_t = u_cosine_similarity.transpose()

kNN = sp.lil_matrix((R.shape[0],R.shape[0]))
total = 0.
total_k = 0.
bsz = args.bsz
loops = 0
#Calculating similarities in blocks
for i in range(0, int(max((nb_users/bsz)+1,1))):
    start = time.perf_counter() 
    i_start = i*bsz
    i_end = i*bsz + bsz
    S = u_cosine_similarity[i_start:i_end,:].dot(u_cos_sim_t)
    duration = time.perf_counter() - start
    total += duration     
    start_k = time.perf_counter()
    block=topk(S, args.k)
    kNN[i_start : i_end, :] = block
    total_k = time.perf_counter()-start_k
    loops += 1

S = None
gc.collect()
if args.test1:
    print("Total time to compute similarities is {} and average time is {}".format(total,total/loops))
    print("Total time to compute top k neighbours is {} and average time is {}".format(total_k,total_k/loops))

kNN = kNN.tocsr()

distances_entire = kNN[0].toarray()
indices_sorted = np.flip(np.argsort(distances_entire))
indices_s = indices_sorted[0,0:args.k]
distances = distances_entire[0, indices_s]

nb_runs = 1
total = 0.
if args.test2:
    nb_runs = 100

watched = collections.defaultdict(dict)
for i in ratings.values.tolist():
    watched[i[0]][i[1]] = i[2]

for i in range (nb_runs):

    # Randomizing user for which the recommendation is going to be done
    if args.seed and args.test1:
        random.seed(0)
        user = random.randint(1,R.shape[0])
        user_index = user -1
    elif args.seed and args.test2:
        random.seed(i)
        user = random.randint(1,R.shape[0]) 
        user_index = user - 1
    elif not args.seed:
        user = int(input("Input the user index for which you'd like to make a recommendation [1,{}]: ".format(R.shape[0])))
        if user < 1 or user > 943:
            print("User index out of bounds")
            sys.exit(1)
        user_index = user - 1  


    start_prediction = time.perf_counter()

    user_watched = set(watched[user_index+1])

    neighbours_watched = {}
    for i in range(0, len(distances)):

        if args.debug:
            if i == 0:
                print()
                print('Closest users to user {}:\n'.format(user_index))
            else:
                print('{0}: {1} - distance: {2}'.format(i, indices_s[i] , 1 - distances[i]))

        # To neighbours_watched dictionary add the set of movies that each of the 20 closest neighbours has watched along with their ratings.
        neighbours_watched[indices_s[i] + 1] = watched[indices_s[i]].copy()

        # Save information in order to calculate predicted rating.
        for key, v in neighbours_watched[indices_s[i]+1].items():
            neighbours_watched[indices_s[i]+1][key] = [1 - distances.flatten()[i], v]

    unwatched_films = []
    for u in neighbours_watched:
        neighbour_watched_user_unwatched = neighbours_watched[u].keys() - user_watched.intersection(neighbours_watched[u].keys())
        for f in neighbour_watched_user_unwatched:
                unwatched_films.append(f)

    common_unwatched = [item for item, count in collections.Counter(unwatched_films).items() if count > 5]

    common_unwatched_rating = []
    for f in common_unwatched:
            m = []
            w = []

            # Iterating over nested dictionaries in the neighbours_watched set.
            for u in neighbours_watched:
                # Condition checking if film 'f' is found in dictionary u for particular neighbour.
                if neighbours_watched[u].get(f) is not None:
                    # Append result of multiplying the rating given for film 'f' by current neighbour and the similarity of said neighbour to user to set 'm'.
                    m.append(neighbours_watched[u].get(f)[0]*neighbours_watched[u].get(f)[1])
                    # Append similarity value of current neighbour to the user for which we are predicting values to set 'w'.
                    w.append(neighbours_watched[u].get(f)[0])

            # Once iterated through all users and found those who have watched film 'f' calculate the predicted rating.
            # Predicted rating = (sum of all ratings given by neighbours for film 'f' * similarity score of said neighbour to user) / (sum of all the similarities of neighbours)
            common_unwatched_rating.append([np.sum(m)/np.sum(w), f])

        # Sort the films unwatched by current user but watched by neighbours from highest to lowest predicted rating.    
    common_unwatched_rating = sorted(common_unwatched_rating, reverse=True)

    duration = time.perf_counter() - start_prediction
    total += duration  
    

    if args.debug :
            print("\n------------------------\n")
            print('10 best recommendations based on what similar users liked:\n')
            for f in common_unwatched_rating[:10]:
                print('{0} - {1} - {2:.2f}'.format(f[1], film.loc[film['filmId'] == f[1]]['title'].values[0], f[0]))
            print()
if args.test1:
    print("Total time taken to make prediction for user {} is {}".format(user_index+1, total))

if args.test2:
    print("Time taken to make predictions for 100 users is {} seconds".format(total))
    print("Average time taken to make a prediction for a single user is {} seconds".format(total/100))

if args.test1:
    memoryUse = python_process.memory_info()[0]/2.**30
    print("Memory space used by process is:",memoryUse, "gigabytes")
print()
print("|--------------------|")
print("|  Process finished  |")
print("|--------------------|")