import collections
import random
import numpy as np 
import pandas as pd
from scipy.sparse import csr_matrix
from sklearn.neighbors import NearestNeighbors
import sys
import time
import os
import psutil
import argparse


parser = argparse.ArgumentParser(description="Used to debug")
parser.add_argument("--test1", default = False,action = "store_true",help="Runs the first test printing time taken for specific parts of the code.")
parser.add_argument("--test2", default = False,action = "store_true",help="Runs the second test printing time taken to make predictions for 100 users.")
parser.add_argument("-s", "--seed", default=False, action="store_true", help="Input a seed for testing.")
parser.add_argument("-d", "--debug", default=False, action= "store_true", help="Print recommendations and distances of neighbours.")
args = parser.parse_args()


# Get current process ID.
pid = os.getpid() 
# For this process get psutil methods.
python_process = psutil.Process(pid)
# Variable to store starting time to measure duration of entire program.
prog_start = time.time()


# Dataframe for file with the movies for which users gave rating.
# only use first 3 columns
rating = pd.read_csv('datasets/ml-100k/u.data', sep='\t', header=None, 
        names=['userId', 'filmId', 'rating', 'timestamp'], 
        engine='python')

# Dataframe for file with the movies ID's and its title.
# Used to make film recommendations later on.
# Load relevant files into dataFrame
film = pd.read_csv('datasets/ml-100k/u.item', sep='|', encoding='ISO-8859-1')
film.columns = ['filmId', 'title', 'release date', 'video release date',
              'IMDb URL', 'unknown', 'Action', 'Adventure', 'Animation',
              'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama',
              'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance',
               'Sci-Fi', 'Thriller', 'War', 'Western']
columns = ['release date', 'video release date',
          'IMDb URL', 'unknown', 'Action', 'Adventure', 'Animation',
          'Children\'s', 'Comedy', 'Crime', 'Documentary', 'Drama',
          'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance',
           'Sci-Fi', 'Thriller', 'War', 'Western']
film = film.drop(columns, axis=1)
# Creating datafram containing all ratings for every user in the dataset.
# Movies for which the user didn't give a rating are given a rating of 0. 
rating_pivot = rating.pivot(index='userId', columns = 'filmId', values='rating').fillna(0)

if args.test1:
    print("Time taken to load data:", time.time()-prog_start, "s")

start_fitting_knn = time.time()
# Convert the pivot table into a sparse matrix
rating_matrix = csr_matrix(rating_pivot.values)


# Initialise k nearest neighbours
knn = NearestNeighbors(metric = 'cosine', algorithm = 'brute')

# Fit the KNN model to the sparse matrix created above.
knn.fit(rating_matrix)

if args.test1:
    print("Time taken to fit KNN model:", time.time()- start_fitting_knn, "s")

# Number of neighbours.
k = 20


# Variable storing number of users in the dataset.
number_of_users = rating_pivot.values.shape[0]

nb_runs = 1
total = 0.
if args.test2:
    nb_runs = 100

# Created a nested dictionary of each user with the filmIds and the ratings
# they provided '1 : {181: 4, 258: 2, ...}, 2 : {7: 5, 13: 3, ...}, ...'
watched = collections.defaultdict(dict)
for i in rating.values.tolist():
    watched[i[0]][i[1]] = i[2]

for i in range(nb_runs):
    # Randomizing user for which the recommendation is going to be done
    if args.seed and args.test1:
        random.seed(0)
        user = random.randint(0,number_of_users -1)
        user_index = user - 1 
    elif args.seed and args.test2:
        random.seed(i)
        user = random.randint(0,number_of_users -1) 
        user_index = user
    elif not args.seed:
        user = int(input("Input the user index for which you'd like to make a recommendation [1,{}]: ".format(rating_pivot.shape[0])))
        if user < 1 or user > 943:
            print("User index out of bounds")
            sys.exit(1)
        user_index = user - 1  


    # Find nearest neighbours for given randomized user.
    distances, indices = knn.kneighbors(rating_pivot.iloc[user_index , :].values.reshape(1, -1), n_neighbors = k)

    start_prediction = time.perf_counter()

    
    # Set films the user 'user_index' has watched.
    user_watched = set(watched[rating_pivot.index[user_index]])


    # Dictionary with nested dictionaries of all the movies that each of the 20 closest neighbours has watched.
    # {1 : {1: [0.45768849, 3], 2: [0.45768849, 4], ...}, 2 : {5: [0.99999999, 3], 4: [0.99999999, 2],....}, ...}
    neighbours_watched = {}

    # Iterate through number of neigbours to print distances and add their movies along with the rating to set 'neighbours_watched'.
    for i in range(0, len(distances.flatten())):

        if args.debug:
            if i == 0:
                print('Closest users to user {}:\n'.format(rating_pivot.index[user_index]))
            else:
                print('{0}: {1} - distance: {2}'.format(i, rating_pivot.index[indices.flatten()[i]], distances.flatten()[i]))

        
        # To neighbours_watched dictionary add the set of movies that each of the 20 closest neighbours has watched along with their ratings.
        neighbours_watched[rating_pivot.index[indices.flatten()[i]]] = watched[rating_pivot.index[indices.flatten()[i]]].copy()

        # Save information in order to calculate predicted rating.
        for key, v in neighbours_watched[rating_pivot.index[indices.flatten()[i]]].items():
            neighbours_watched[rating_pivot.index[indices.flatten()[i]]][key] = [1 - distances.flatten()[i], v]


    # Set of movies watched by neighbours but not by user (may contain duplicates).
    unwatched_films = []
    for u in neighbours_watched:
        # Films that the neighbours has watched but that user has not.
        neighbour_watched_user_unwatched = neighbours_watched[u].keys() - user_watched.intersection(neighbours_watched[u].keys())
        for f in neighbour_watched_user_unwatched:
            unwatched_films.append(f)

    # Set of films that have been watched by more than 5 neighbours but that current user has not watched.
    common_unwatched = [item for item, count in collections.Counter(unwatched_films).items() if count > 5]

    # Set of prredicted ratings for current user on the common_unwatched films.
    common_unwatched_rating = []

    # Iterating over films in the common_unwatched set.
    for f in common_unwatched:
        m = []
        w = []

        # Iterating over nested dictionaries in the neighbours_watched set.
        for u in neighbours_watched:
            # Condition checking if film 'f' is found in dictionary u for particular neighbour.
            if neighbours_watched[u].get(f) is not None:
                # Append result of multiplying the rating given for film 'f' by current neighbour and the similarity of said neighbour to user to set 'm'.
                m.append(neighbours_watched[u].get(f)[0]*neighbours_watched[u].get(f)[1])
                # Append similarity value of current neighbour to the user for which we are predicting values to set 'w'.
                w.append(neighbours_watched[u].get(f)[0])

        # Once iterated through all users and found those who have watched film 'f' calculate the predicted rating.
        # Predicted rating = (sum of all ratings given by neighbours for film 'f' * similarity score of said neighbour to user) / (sum of all the similarities of neighbours)
        common_unwatched_rating.append([np.sum(m)/np.sum(w), f])

    # Sort the films unwatched by current user but watched by neighbours from highest to lowest predicted rating.    
    common_unwatched_rating = sorted(common_unwatched_rating, reverse=True)
    duration = time.perf_counter() - start_prediction
    total += duration  
    if args.debug :
        print("\n------------------------\n")
        print('10 best recommendations based on what similar users liked:\n')
        for f in common_unwatched_rating[:10]:
            print('{0} - {1} - {2:.2f}'.format(f[1], film.loc[film['filmId'] == f[1]]['title'].values[0], f[0]))


if args.test2:
    print("Time taken to make predictions for 100 users is {} seconds".format(total))
    print("Average time taken to make a prediction for a single user is {} seconds".format(total/100))


if args.test1:
    print("Total time taken to make prediction for user {} is {}".format(user_index, total))
    memoryUse = python_process.memory_info()[0]/2.**30
    print("Memory space used by process is:",memoryUse, "gigabytes")

if args.test1 or args.test2:
    print("Time taken to execute whole prgram:" , time.time()- prog_start,"s")
print()
print("|--------------------|")
print("|  Process finished  |")
print("|--------------------|")